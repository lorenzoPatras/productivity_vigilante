// since content.js cannot be used as a module and can't import Keys definitions without
// strange workarounds will redeclare them here.

const Keys = {
  BLOCKED_SITES: 'blocked_sites',
  BLOCK_TIME: 'block_time',
  FROM_TIME: 'from_time',
  UNTIL_TIME: 'until_time',
  HOUR: 'h',
  MINUTE: 'm',
  TIMER: 'timer',
  TIMER_STATE: 'timer_state',
  TIMER_TOTAL_TIME: 'total_time',
  TIMER_REM_TIME: 'remaining_time'
}

const TimerState = {
  START: 'start',
  PAUSE: 'pause',
  STOP: 'stop'
}

block()

function block() {
  chrome.storage.sync.get(null, (result) => {

    let blockTime = result[Keys.BLOCK_TIME] || {
      [Keys.FROM_TIME]: {
        [Keys.HOUR]: 0,
        [Keys.MINUTE]: 0
      },
      [Keys.UNTIL_TIME]: {
        [Keys.HOUR]: 0,
        [Keys.MINUTE]: 0
      }
    };
    let timerActive = result[Keys.TIMER][Keys.TIMER_STATE] || TimerState.STOP;
    let blockUntil = result[Keys.TIMER][Keys.UNTIL_TIME] || Date.now() - 1;
    let blockList = result[Keys.BLOCKED_SITES] || {};
    let hostname = new URL(location.href).hostname;

    if (blockList[hostname] && blockList[hostname].enabled
      && (isBlockingTimeActive(blockTime) || timerActive === TimerState.START && Date.now() < blockUntil)) {
      document.body.innerHTML = createBlockCover().outerHTML;
      applyBlockStyle();
    }
  })
}

function isBlockingTimeActive(blockTime) {
  if (!blockTime) return false;

  let crtTime = new Date();
  let startTime = new Date();
  let endTime = new Date();

  startTime.setHours(blockTime[Keys.FROM_TIME][Keys.HOUR]);
  startTime.setMinutes(blockTime[Keys.FROM_TIME][Keys.MINUTE]);
  endTime.setHours(blockTime[Keys.UNTIL_TIME][Keys.HOUR]);
  endTime.setMinutes(blockTime[Keys.UNTIL_TIME][Keys.MINUTE]);

  return startTime < crtTime && crtTime < endTime;
}

function applyBlockStyle() {
  var link = document.createElement("link");
  link.href = chrome.runtime.getURL("/src/css/block.css");
  link.type = "text/css";
  link.rel = "stylesheet";
  document.getElementsByTagName("head")[0].appendChild(link);
  document.querySelector('link[rel*="icon"]').href = chrome.runtime.getURL('/img/vigilante_128.png');
}

function createBlockCover() {
  let div = document.createElement('div');
  let img = document.createElement('img');
  img.src = chrome.runtime.getURL('/img/focus.jpg');

  div.appendChild(img);
  div.appendChild(createDisclaimer());

  return div;
}

function createDisclaimer() {
  let div = document.createElement('div');
  div.id = 'disclaimer';
  let p = document.createElement('p');
  p.innerText = 'You have blocked this site via Vigilante extension. ' +
    'To unblock it or for other settings, check the extension options or view the block list.';

  div.appendChild(p);

  return div;
}

