import { MessageTypes, Keys, TimerState } from "./js/modules/consts.mjs";
import { StorageManager } from "./js/modules/storage_manager.mjs"
import { TabManager } from "./js/modules/tab_manager.mjs"


const storageManager = new StorageManager();
const tabManager = new TabManager();
const STORAGE_KEY = 'vigilante'

chrome.runtime.onConnect.addListener((port) => {
  console.assert(port.name === 'timer');
  port.onMessage.addListener(handlePortMessage);
});

chrome.runtime.onInstalled.addListener((_) => {
  storageManager.getBlockedTime((res) => {
    if (!res) {
      storageManager.setBlockTime(["00", "00"], ["00", "00"]);
    }
  })
})

function handlePortMessage(msg, port) {
  switch (msg.type) {
    case MessageTypes.INIT_TIMER:
      storageManager.getTimerInfo((info) => {
        port.postMessage({ // todo get from storage manager
          type: MessageTypes.INIT_TIMER,
          load: info
        });
      })

      break;
    case MessageTypes.TIMER_ACTION:
      console.assert(msg.load);
      storageManager.setTimerInfo(msg.load);
      storageManager.getBlockedList((list) => {
        tabManager.refreshTabs(list);
      })
      break;
    default:
      console.error(`Message type ${msg.type} unknown.`);
  }
}

chrome.alarms.onAlarm.addListener((alarm) => {
  if (alarm.name === 'vigilante') {
    chrome.alarms.clear('vigilante');
    storageManager.setTimerInfo({
      state: TimerState.STOP,
      remainingTime: 0,
      totalTime: 0,
      blockUntil: Date.now()
    });
    storageManager.getBlockedList((list) => {
      tabManager.refreshTabs(list);
    })
  }
})