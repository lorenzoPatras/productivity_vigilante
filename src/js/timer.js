import { MessageTypes, TimerState } from "./modules/consts.mjs";

const port = chrome.runtime.connect({ name: 'timer' });
port.onMessage.addListener(handleMessageFromBackground);

const BtnTexts = {
  START: 'Start',
  PAUSE: 'Pause',
  RESUME: 'Resume',
  STOP: 'Stop'
}

const states = {
  [TimerState.START]: {
    startBtnTxt: BtnTexts.START,
    stopBtnTxt: BtnTexts.PAUSE,
    startBtnDisabled: true,
    stopBtnDisabled: false
  },
  [TimerState.PAUSE]: {
    startBtnTxt: BtnTexts.RESUME,
    stopBtnTxt: BtnTexts.STOP,
    startBtnDisabled: false,
    stopBtnDisabled: false
  },
  [TimerState.STOP]: {
    startBtnTxt: BtnTexts.START,
    stopBtnTxt: BtnTexts.STOP,
    startBtnDisabled: false,
    stopBtnDisabled: true
  }
}

let currentState = TimerState.STOP;
let totalTime = 0;
let remainingTime = 0;
let intervalId = -1;

function initializeTimer() {
  port.postMessage({ type: MessageTypes.INIT_TIMER });
}

initializeTimer();

const startBtn = document.getElementById("start");
startBtn.addEventListener("click", (event) => {
  if (currentState === TimerState.STOP) {
    remainingTime = totalTime = readTime();
  }
  start();
});

const stopBtn = document.getElementById("stop_pause");
stopBtn.addEventListener("click", (event) => {
  if (currentState === TimerState.START) {
    pause();
  } else if (currentState === TimerState.PAUSE) {
    stop();
  }
})

function createLoad(state, remTime, totTime) {
  let blockUntil = Date.now() + remTime * 1000;
  return {
    state: state,
    remainingTime: remTime,
    totalTime: totTime,
    blockUntil: blockUntil
  }
}

function start() {
  if (totalTime === 0) return;

  if (totalTime) {
    port.postMessage({
      type: MessageTypes.TIMER_ACTION,
      load: createLoad(TimerState.START, remainingTime, totalTime)
    });
  }

  chrome.alarms.create('vigilante', { when: Date.now() + remainingTime * 1000 });
  intervalId = setInterval(update, 1000);
  updateCurrentState(TimerState.START);
}

function pause() {
  port.postMessage({
    type: MessageTypes.TIMER_ACTION,
    load: createLoad(TimerState.PAUSE, remainingTime, totalTime)
  });
  clearInterval(intervalId);
  chrome.alarms.clear('vigilante');
  updateCurrentState(TimerState.PAUSE);
}

function stop() {
  port.postMessage({
    type: MessageTypes.TIMER_ACTION,
    load: createLoad(TimerState.STOP, 0, 0)
  });

  remainingTime = totalTime = 0;
  clearInterval(intervalId);
  chrome.alarms.clear('vigilante');
  updateCurrentState(TimerState.STOP);
}

function updateCurrentState(newState) {
  currentState = newState;
  updateDOMState(currentState);
  updateDOM();
}

function updateDOMState(newState) {
  currentState = newState;
  const domState = states[newState];
  startBtn.disabled = domState.startBtnDisabled;
  stopBtn.disabled = domState.stopBtnDisabled;
  startBtn.innerText = domState.startBtnTxt;
  stopBtn.innerText = domState.stopBtnTxt;
}

function readTime() {
  const minElem = document.getElementById("min");
  const secElem = document.getElementById("sec");
  const min = minElem.value ? parseInt(minElem.value) : 0;
  const sec = secElem.value ? parseInt(secElem.value) : 0;
  return min * 60 + sec;
}

function handleMessageFromBackground(msg) {
  switch (msg.type) {
    case MessageTypes.INIT_TIMER:
      console.assert(msg.load);
      if (currentState != msg.load.timerState) {
        updateCurrentState(msg.load.timerState);
      }

      if (currentState === TimerState.START) {
        remainingTime = Math.floor((new Date(msg.load.blockUntil) - Date.now()) / 1000)
        intervalId = setInterval(update, 1000);
      } else if (currentState === TimerState.PAUSE) {
        remainingTime = msg.load.remainingTime;
      }

      totalTime = msg.load.totalTime;
      updateDOM();

      break;
    default:
      console.error(`Cannot handle msg type ${msg.type}`);
  }
}

function update() {
  if (remainingTime <= 0) {
    stop();
    return;
  }
  remainingTime--;
  updateDOM();
}

function updateDOM() {
  updateMinSec();
  updatePercentage();
}

function updateMinSec() {
  const remMin = Math.floor(remainingTime / 60);
  const remSec = remainingTime % 60;
  document.getElementById("min").value = remMin;
  document.getElementById("sec").value = remSec;
}

function updatePercentage() {
  const r = document.querySelector(':root');
  if (totalTime === 0) {
    r.style.setProperty('--fill_percentage', 0);
  }
  else {
    r.style.setProperty('--fill_percentage', remainingTime / totalTime * 100);
  }
}