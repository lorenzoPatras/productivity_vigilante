import { Keys } from "./modules/consts.mjs"
import { StorageManager } from "./modules/storage_manager.mjs"
import { TabManager } from "./modules/tab_manager.mjs"

let hostname = ''
const storageManager = new StorageManager();
const tabManager = new TabManager();

function getWebDomain() {
  tabManager.getWebDomain((host) => {
    web_domain.innerText = hostname = host;
  });
}

const blockSiteBtn = document.getElementById("block_btn");
blockSiteBtn.addEventListener("click", (e) => {
  storageManager.addBlockedSite(hostname);
  tabManager.refreshTabsWithHostname(hostname);
})

const blockListBtn = document.getElementById("list_btn");
blockListBtn.addEventListener("click", (e) => {
  chrome.runtime.openOptionsPage();
})

getWebDomain();

