const MessageTypes = {
  INIT_TIMER: 'init_timer',
  TIMER_ACTION: 'timer_action',
}

const TimerState = {
  START: 'start',
  PAUSE: 'pause',
  STOP: 'stop'
}

// keep in sync with content.js
const Keys = {
  BLOCKED_SITES: 'blocked_sites',
  BLOCK_TIME: 'block_time',
  FROM_TIME: 'from_time',
  UNTIL_TIME: 'until_time',
  HOUR: 'h',
  MINUTE: 'm',
  TIMER: 'timer',
  TIMER_STATE: 'timer_state',
  TIMER_TOTAL_TIME: 'total_time',
  TIMER_REM_TIME: 'remaining_time'
}

export {
  MessageTypes,
  TimerState,
  Keys
}