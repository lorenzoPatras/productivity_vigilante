import { Keys, TimerState } from './consts.mjs'

class StorageManager {
  constructor() { }

  addBlockedSite(hostname) {
    chrome.storage.sync.get([Keys.BLOCKED_SITES], (res) => {
      let blockedList = res[Keys.BLOCKED_SITES] ? res[Keys.BLOCKED_SITES] : {};
      blockedList[hostname] = { enabled: true };
      chrome.storage.sync.set({ [Keys.BLOCKED_SITES]: blockedList });
    });
  }

  setEnableStateForHost(host, enable) {
    chrome.storage.sync.get([Keys.BLOCKED_SITES], (res) => {
      let blockedList = res[Keys.BLOCKED_SITES] ? res[Keys.BLOCKED_SITES] : {};
      console.assert(blockedList[host]);
      blockedList[host].enabled = enable;
      chrome.storage.sync.set({ [Keys.BLOCKED_SITES]: blockedList });
    });
  }

  setTimerInfo(timerInfo) {
    console.log('set timer info ' + JSON.stringify(timerInfo));
    chrome.storage.sync.set({
      [Keys.TIMER]: {
        [Keys.TIMER_STATE]: timerInfo.state,
        [Keys.TIMER_REM_TIME]: timerInfo.remainingTime,
        [Keys.TIMER_TOTAL_TIME]: timerInfo.totalTime,
        [Keys.UNTIL_TIME]: timerInfo.blockUntil
      }
    });
  }

  setBlockTime(fromTime, untilTime) {
    chrome.storage.sync.set({
      [Keys.BLOCK_TIME]: {
        [Keys.FROM_TIME]: {
          [Keys.HOUR]: fromTime[0],
          [Keys.MINUTE]: fromTime[1]
        },
        [Keys.UNTIL_TIME]: {
          [Keys.HOUR]: untilTime[0],
          [Keys.MINUTE]: untilTime[1]
        }
      }
    });
  }

  getBlockedList(callback) {
    chrome.storage.sync.get([Keys.BLOCKED_SITES], (res) => {
      if (res[Keys.BLOCKED_SITES]) {
        callback(res[Keys.BLOCKED_SITES]);
      } else {
        callback({});
      }
    });
  }

  getBlockedTime(callback) {
    chrome.storage.sync.get([Keys.BLOCK_TIME], (res) => {
      if (res[Keys.BLOCK_TIME]) {
        callback(res[Keys.BLOCK_TIME]);
      } else {
        callback();
      }
    });
  }

  getTimerInfo(callback) {
    chrome.storage.sync.get([Keys.TIMER], (res) => {
      if (!res[Keys.TIMER] || res[Keys.TIMER][Keys.TIMER_STATE] === TimerState.STOP) {
        callback({
          timerState: TimerState.STOP,
          remainingTime: 0,
          totalTime: 0,
          blockUntil: Date.now()
        })
      } else {
        callback({
          timerState: res[Keys.TIMER][Keys.TIMER_STATE],
          remainingTime: res[Keys.TIMER][Keys.TIMER_REM_TIME],
          totalTime: res[Keys.TIMER][Keys.TIMER_TOTAL_TIME],
          blockUntil: res[Keys.TIMER][Keys.UNTIL_TIME]
        })
      }
    });
  }

  removeFromBlocked(host, callback) {
    chrome.storage.sync.get([Keys.BLOCKED_SITES], (res) => {
      if (!res[Keys.BLOCKED_SITES]) return;

      let blockedList = res[Keys.BLOCKED_SITES];
      console.assert(blockedList[host]);
      delete blockedList[host];
      chrome.storage.sync.set({ [Keys.BLOCKED_SITES]: blockedList }, callback);
    })
  }

  removeAllBlockedSites(callback) {
    chrome.storage.sync.set({ [Keys.BLOCKED_SITES]: {} }, callback);
  }

  removeAllVigilanteData() {
    Object.keys(Keys).forEach((key) => {
      chrome.storage.sync.remove(Keys[key]);
    })
  }

  exportSettings() {
    chrome.storage.sync.get(null, (res) => {
      let url = 'data:application/json;base64,' + btoa(JSON.stringify(res));
      chrome.downloads.download({
        url: url,
        filename: 'vigilante.json'
      });
    });
  }

  importSettings(settings) {
    chrome.storage.sync.set(JSON.parse(settings));
  }

  __debugShowEntireStorage() {
    chrome.storage.sync.get(null, (res) => {
      console.log(JSON.stringify(res, null, 2));
    })
  }
}

export { StorageManager }