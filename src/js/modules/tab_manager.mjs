class TabManager {
  constructor() { }

  refreshTabsWithHostname(hostname) {
    chrome.tabs.query({}, (tabs) => {
      tabs.forEach((tab) => {
        const domain = new URL(tab.url);
        if (domain.hostname === hostname) {
          chrome.tabs.reload(tab.id);
        }
      });
    })
  }

  refreshTabs(hosts) {
    let hostsList = Object.keys(hosts);
    chrome.tabs.query({}, (tabs) => {
      tabs.forEach((tab) => {
        const domain = new URL(tab.url);
        if (hostsList.includes(domain.hostname)) {
          chrome.tabs.reload(tab.id);
        }
      });
    })
  }

  getWebDomain(callback) {
    chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
      const domain = new URL(tabs[0].url);
      callback(domain.hostname);
    })
  }
}

export { TabManager }