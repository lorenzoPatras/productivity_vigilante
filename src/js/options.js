import { Keys } from "./modules/consts.mjs"
import { StorageManager } from "./modules/storage_manager.mjs"
import { TabManager } from "./modules/tab_manager.mjs"

const storageManager = new StorageManager();
const tabManager = new TabManager();

const fileSelector = document.getElementById('file_selector');
fileSelector.addEventListener('change', (e) => {
  var reader = new FileReader();
  reader.onload = (ev) => {
    storageManager.importSettings(ev.target.result);
    location.reload();
  }
  reader.readAsText(e.target.files[0]);
});

const exportBtn = document.getElementById('export');
exportBtn.addEventListener('click', (e) => {
  storageManager.exportSettings();
});

const removeBtn = document.getElementById('remove_all');
removeBtn.addEventListener('click', (e) => {
  storageManager.getBlockedList((list) => {
    let blockList = Object.keys(list);
    storageManager.removeAllBlockedSites(() => {
      blockList.forEach((host) => {
        tabManager.refreshTabsWithHostname(host);
      });
      location.reload();
    });
  })
})

const applyBtn = document.getElementById('apply');
applyBtn.addEventListener('click', (e) => {
  const fromTime = document.getElementById("from").value.split(':');
  const untilTime = document.getElementById("until").value.split(':');

  console.assert(fromTime, "from time unset");
  console.assert(untilTime, "until time unset");

  storageManager.setBlockTime(fromTime, untilTime);
  storageManager.getBlockedList((list) => {
    let blockList = Object.keys(list);
    storageManager.getBlockedList(() => {
      blockList.forEach((host) => {
        tabManager.refreshTabsWithHostname(host);
      });
      location.reload();
    });
  })
});

function pupulateBlockList() {
  storageManager.getBlockedList((blockList) => {
    createDivsForBlockedSites(blockList);
  })
}

function initializeBlockTime() {
  storageManager.getBlockedTime((result) => {
    const fromTime = document.getElementById("from");
    const untilTime = document.getElementById("until");

    fromTime.value = result[Keys.FROM_TIME][Keys.HOUR] + ":" + result[Keys.FROM_TIME][Keys.MINUTE];
    untilTime.value = result[Keys.UNTIL_TIME][Keys.HOUR] + ":" + result[Keys.UNTIL_TIME][Keys.MINUTE];
  })
}

function createDivsForBlockedSites(blockList) {
  const divs = Object.keys(blockList).map((key) => {
    return createDiv(key, blockList[key]);
  });
  let blockedSites = document.getElementById(Keys.BLOCKED_SITES);
  divs.forEach((div) => { blockedSites.appendChild(div); })
}

function createDiv(hostname, data) {
  const div = document.createElement('div');
  div.classList.add('site');

  const p = document.createElement('p');
  p.innerText = hostname;
  div.appendChild(p);

  div.appendChild(createSlider(hostname, data));

  const deleteImg = document.createElement('img');
  deleteImg.src = '../../img/trash.png';
  deleteImg.classList.add('deleteImg');

  deleteImg.addEventListener('click', () => {
    removeHost(hostname);
    tabManager.refreshTabsWithHostname(hostname);
  })
  div.appendChild(deleteImg);

  return div;

  function createSlider(host, data) {
    const sliderBtn = document.createElement('label');
    sliderBtn.classList.add('switch');
    const input = document.createElement('input');
    input.type = 'checkbox';
    input.checked = data.enabled;
    input.addEventListener('click', (e) => {
      setEnableForHost(host, input.checked);
      tabManager.refreshTabsWithHostname(host);
    })
    const span = document.createElement('span');
    span.classList.add('slider');
    span.classList.add('round');
    sliderBtn.appendChild(input);
    sliderBtn.appendChild(span);
    return sliderBtn;
  }
}

function setEnableForHost(host, enable) {
  storageManager.setEnableStateForHost(host, enable);
}

function removeHost(host) {
  storageManager.removeFromBlocked(host, () => {
    location.reload();
  });
}

pupulateBlockList();
initializeBlockTime();
